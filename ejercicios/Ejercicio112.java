import java.util.Scanner;

public class Ejercicio112 {
    public static Scanner scanner = new Scanner(System.in);
    public static int maxkm;
    public static void main(String[] var0) {

        try{
            while(maxkm!=777515){
                String res = output();
                if(res=="NULL"){
                    break;
                }
                System.out.println(res);
            }
        }catch(Exception e){
            System.out.println(e.getCause());
        }


    }

    public static int[] lineas(){
        int[] entradas  = new int[3];
        entradas[0] = scanner.nextInt();
        entradas[1] = scanner.nextInt();
        entradas[2] = scanner.nextInt();
        return entradas;

    }

    public static double obtenervelocidadmetros(){
        int[] entradas = lineas();
        double distancia =  entradas[0];
        maxkm = entradas[1];
        double tiempo = entradas[2];
        double velocidad = distancia/tiempo;
        if(entradas[0] <= 0||entradas[1] <= 0||entradas[2] <= 0){
            maxkm = 777516;
        }
        if(entradas[0] == 0&&entradas[1] == 0&&entradas[2] == 0){
            maxkm = 777517;
        }
        return velocidad;
    }

    public static double kilometros(){
        double m = obtenervelocidadmetros();
        double km = m*3.6;
        return km;

    }
    
    public static String output(){
        double km = kilometros();
        String resultado = "";
        if(km>maxkm){
            if (maxkm == 777516){
                resultado = "ERROR";
            }else if(km>maxkm+(maxkm*0.20)){
                resultado = "PUNTOS";

            }else{
                resultado = "MULTA";
            }
        }
        else if (maxkm == 777516){
            resultado = "ERROR";
        }
        else if (maxkm == 777517){
            resultado = "NULL";
        }
        else{
            resultado = "OK";
        }
        return resultado;
    }
}
