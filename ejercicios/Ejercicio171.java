import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio171 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int total = sc.nextInt();
        int altura;

        while (total != 0) {
            int max = 0;
            ArrayList<Integer> solet = new ArrayList();
            for (int i = 0; i < total; i++) {
                altura = sc.nextInt();
                if (altura >= max) {
                    solet.clear();
                    max = altura;
                } else if (altura >= solet.get(solet.size() - 1)) {
                    int index = solet.size() - 1;
                    while (solet.get(index) <= altura) {
                        solet.remove(index);
                        index--;
                    }
                }
                solet.add(altura);
            }
            System.out.println(solet.size());
            total = sc.nextInt();

        }
    }
}
