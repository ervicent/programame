import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio477 {

    public static void main(String[] args) {
        int narmes, inocents, dolent;
        Scanner entrada = new Scanner(System.in);
        int vida = entrada.nextInt();
        while (vida != 0) { //Cal canviar el 0 pel valor corresponent
            //código programa
            narmes = entrada.nextInt();
            ArrayList<ArrayList<Integer>> armes = new ArrayList<ArrayList<Integer>>();
            for (int i = 0; i < 3; i++) {
                armes.add(new ArrayList<Integer>());
            }

            //llegir i ordenar armes
            int sumatotal = 0;
            for (int i = 0; i < narmes; i++) {
                inocents = entrada.nextInt();
                dolent = entrada.nextInt();
                sumatotal += dolent;
                ColocarOrdenat(armes, inocents, dolent, i+1);
            }
            //Mostrar valors
            if (sumatotal >= vida) {
                int suma = 0;
                
                for (int i = 0; i < armes.get(1).size()  && suma < vida;i++){
                    System.out.print(armes.get(2).get(i));
                    suma += armes.get(1).get(i);
                    if (suma < vida) {
                        System.out.print(" ");
                    }
                }
                System.out.println();
            } else {
                System.out.println("MUERTE ESCAPA");
            }
            //següent valor
            vida = entrada.nextInt();
        }
    }

    public static void ColocarOrdenat(ArrayList<ArrayList<Integer>> armes, int valor1, int valor2, int ordre) {
        boolean trobat = false;

        for (int i = 0; i < armes.get(0).size() && !trobat; i++) {
            if ((valor1 < armes.get(0).get(i))||(valor1 == armes.get(0).get(i)&&valor2 > armes.get(1).get(i))) {
                armes.get(0).add(i, valor1);
                armes.get(1).add(i, valor2);
                armes.get(2).add(i, ordre);
                trobat = true;
            }
        }

        if (!trobat) { //Si no l'ha trobat l'afegim al final
            armes.get(0).add(valor1);
            armes.get(1).add(valor2);
            armes.get(2).add(ordre);
        }
    }

}
