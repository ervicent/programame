import java.util.Scanner;
public class Ejercicio219 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int total = sc.nextInt();
        for (int i = 0; i < total; i++) {
            int conta=0;
            int numdecims=sc.nextInt();
            for(int j=0;j<numdecims;j++){
                int loto=sc.nextInt();
                if(loto%2==0)
                    conta++;
            }
            System.out.println(conta);
        }
    }
}

