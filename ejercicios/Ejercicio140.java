
import java.util.Scanner;

public class Ejercicio140 {
	public static Scanner scanner = new Scanner(System.in);
	public static String ESPACIO = " ";
	public static String SUMA = "+";
	public static String IGUAL = "=";
	
	public static void main(String[] var0) {
		try {
			String strNumero = Ejercicio140.lecturaLinea();
			long numero = Long.parseLong(strNumero);

			while (validarNumero(numero)) {

				long sumaDigitos = 0L;
				for (int i = 0; i < strNumero.length(); i++) {
					String digito = String.valueOf(strNumero.charAt(i));
					sumaDigitos += Integer.parseInt(digito);
					if (i == strNumero.length()-1){
						System.out.print(digito + ESPACIO + IGUAL);
					}
					else {
						System.out.print(digito + ESPACIO + SUMA + ESPACIO);
					}
				}

				System.out.println(ESPACIO + sumaDigitos);

				strNumero = Ejercicio140.lecturaLinea();
				numero = Long.parseLong(strNumero);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String lecturaLinea() {
		String linea = scanner.nextLine();
		return linea;
	}

	public static boolean validarNumero(long numero) {

		if (numero < 0) {
			return false;
		} else {
			return true;
		}

	}

}
