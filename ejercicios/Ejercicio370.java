    import java.util.Scanner;


    public class Ejercicio370 {
        public static Scanner scanner = new Scanner(System.in);
        public static void main(String[] var0){
            try{
                int casos = casosprueba();
                for(int i = 0; i<casos; i++){
                    esreal();

                }
            }catch(Exception e){
                System.out.println(e.getCause());
            }
        }    
        public static int casosprueba(){
            int casos = scanner.nextInt();
            scanner.nextLine();
            return casos;
        }
        public static int[] entrada(){
            String linea = scanner.nextLine();
            
                String[] formato = linea.split("-");
                int[] extremo = new int[2];
                extremo[0] = Integer.valueOf(formato[0]);
                extremo[1] = Integer.valueOf(formato[1]);
                return extremo;
            
        }
        public static void esreal(){
            int[] extremo = entrada();
                if(extremo[0]==extremo[1]+1||extremo[0]==extremo[1]-1){
                    if(extremo[0]<extremo[1]){
                        if(extremo[0]%2==0){
                            System.out.println("SI");
                        }else{
                            System.out.println("NO");
                        }
                    }else if(extremo[0]>extremo[1]){
                        if(extremo[1]%2==0){
                            System.out.println("SI");
                        }else{
                            System.out.println("NO");
                        }
                    }
                }else{
                    System.out.println("NO");
                }
            
        }


    }
