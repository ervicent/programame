import java.util.Scanner;

public class Ejercicio139 {

    static int[] llista = new int[100];

    public static void main(String[] args) {
        int xifres, valor, calcul = 0;
        int comptador;
        boolean fi = false;
        Scanner sc = new Scanner(System.in);
        valor = sc.nextInt();
        while (valor != 0) {
            comptador=1;
            llista[comptador-1]=valor;
            if (valor == 1) {
                System.out.println("1 -> cubifinito.");
                fi = true;
            } else {
                System.out.print(valor + " -");
                fi = false;
            }

            while (!(fi)) {
                calcul = 0;
                xifres = valor;
                while (xifres != 0) {
                    int xifra = xifres % 10;
                    calcul = calcul + (xifra * xifra * xifra);
                    xifres = xifres / 10;
                }
                if (calcul == 1) {
                    System.out.println(" 1 -> cubifinito.");
                    fi = true;
                } else if (Existe(calcul,comptador)) {
                    System.out.println(" " + calcul + " -> no cubifinito.");
                    fi = true;
                } else {
                    llista[comptador]=calcul;
                    System.out.print(" " + calcul + " -");
                    valor = calcul;
                    comptador++;
                }
            }
            valor = sc.nextInt(); //Llegeix següent valor
        }
    }

    public static boolean Existe(int num,int comptador) {
        boolean trobat = false;
        for (int i = 0; i < comptador && !trobat; i++) {
            if (llista[i] == num) {
                trobat = true;
            }
        }
        return trobat;
    }
}
