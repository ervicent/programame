import java.util.Scanner;

public class Ejercicio400 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] var0) {
        try{
            int n = 40;
            while(n!=0){
                int libre = camaslibres();
                System.out.println(libre);
            }
        }catch(Exception e){
            System.out.println(e.getCause());
        }

    }
    
    public static String[] obtenercamas(){
        String linea = scanner.nextLine();
        String[] separa = linea.split("");
        return separa;
    }

    public static int camaslibres(){
        String[] camas = obtenercamas();
        int libre = 0;

        for(int i = 0; i<camas.length;i++){
            if(camas[i].equals("x")){
                System.out.println(i+" es x");
            }
            if(camas[i].equals(".")){
                if(i==0){
                    if(!camas[i+1].equals("x")){
                        libre++;
                        System.out.println(libre);
                    }
                }else{
                    if(!camas[i-1].equals("x") && !camas[i+1].equals("x")){
                        libre++;
                        System.out.println(libre);
                    }
                }

            }
        }

        System.out.println("la libre es "+ libre);
        return libre;
    }

}
