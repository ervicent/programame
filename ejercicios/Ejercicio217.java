import java.util.Scanner;

public class Ejercicio217 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] var0) {

        try{
            int n = 32;
            while(n!=0){
                String lado = ladocalle();
                if(lado=="null"){
                    break;
                }else{
                    System.out.println(lado);
                }
            }
        }catch(Exception e){
            System.out.println(e.getCause());
        }

    }
    public static String ladocalle(){
        int casa = scanner.nextInt();

        if(casa==0||casa>1000){ 
            return "null";
        }else if(casa%2==0){
            return "DERECHA";
        }else{
            return "IZQUIERDA";
        }
    }
}
