import java.util.Arrays;
import java.util.Scanner;

public class Ejercicio477_arrays {

    public static void main(String[] args) {
        int narmes, inocents, dolent;
        Scanner entrada = new Scanner(System.in);
        int vida = entrada.nextInt();
        while (vida != 0) { //Cal canviar el 0 pel valor corresponent
            //código programa
            narmes = entrada.nextInt();
            int armes[][] = new int[3][narmes];
            //llegir i ordenar armes
            int sumatotal = 0;
            for (int i = 0; i < narmes; i++) {
                inocents = entrada.nextInt();
                dolent = entrada.nextInt();
                sumatotal += dolent;
                ColocarOrdenat(armes, inocents, dolent, i);
            }
            //Mostrar valors
            if (sumatotal >= vida) {
                int suma = 0;
                for (int i = 0; i < armes[1].length && suma < vida; i++) {
                    System.out.print(armes[2][i]);
                    suma += armes[1][i];
                    if (suma < vida) {
                        System.out.print(" ");
                    }
                }
                System.out.println();
            } else {
                System.out.println("MUERTE ESCAPA");
            }
            //següent valor
            vida = entrada.nextInt();
        }
    }

    public static void ColocarOrdenat(int armes[][], int valor1, int valor2, int ultim) {
        int ordenat[][] = new int[3][ultim + 1];
        boolean trobat = false;
        ordenat[0] = Arrays.copyOf(armes[0], ultim + 1);
        ordenat[1] = Arrays.copyOf(armes[1], ultim + 1);
        ordenat[2] = Arrays.copyOf(armes[2], ultim + 1);

        for (int i = 0; i < ultim; i++) {
            if (!trobat) {
                if (valor1 < armes[0][i]) {
                    trobat = true;
                    ordenat[0][i] = valor1;
                    ordenat[1][i] = valor2;
                    ordenat[2][i] = ultim + 1;
                } else {
                    if (valor1 == armes[0][i] && valor2 > armes[1][i]) {
                        trobat = true;
                        ordenat[0][i] = valor1;
                        ordenat[1][i] = valor2;
                        ordenat[2][i] = ultim + 1;
                    } else {
                        ordenat[0][i] = armes[0][i];
                        ordenat[1][i] = armes[1][i];
                        ordenat[2][i] = armes[2][i];
                    }
                }
            } else {
                ordenat[0][i] = armes[0][i - 1];
                ordenat[1][i] = armes[1][i - 1];
                ordenat[2][i] = armes[2][i - 1];
            }
        }
        if (!trobat) { //Si no l'ha trobat l'afegim al final
            ordenat[0][ultim] = valor1;
            ordenat[1][ultim] = valor2;
            ordenat[2][ultim] = ultim + 1;//posició inicial
        } else {
            ordenat[0][ultim] = armes[0][ultim - 1];
            ordenat[1][ultim] = armes[1][ultim - 1];
            ordenat[2][ultim] = armes[2][ultim - 1];
        }
        armes[0] = Arrays.copyOf(ordenat[0], ultim + 1);
        armes[1] = Arrays.copyOf(ordenat[1], ultim + 1);
        armes[2] = Arrays.copyOf(ordenat[2], ultim + 1);

    }

}
