import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Ejercicio100 {
    public static Scanner scanner = new Scanner(System.in);
    public static char[] charord;
    public static Integer[] input;
    public static Integer[] validante;
    public static boolean primeravez = true;
    public static int res = 0;
    public static void main(String[] var0) {
        
        // 4382 -> 8432 - 2348 = 6084: 8640 - 0468 = 8172: 8721 - 1278 = 7443: 7443 - 3447 = 3996: 9963 - 3699 = 6264: 6642 - 2466 = 4176: 7641 - 1467 = 6174:
           try{
            String x = Ejercicio100.lecturaLinea();
            int n = Integer.parseInt(x);
            int numerocasos = 0;
            while(numerocasos!=n){
                numerocasos++;
                System.out.println(vueltas());
                primeravez = true;
            }
           }catch(Exception e){
                e.printStackTrace();
            }

        }

    public static int vueltas(){
        int vueltas = 0;
        res = 0;
        while(res!=6174){

            if(primeravez){
                int numero = numerocompleto();
                int numero_rvs = numerocompleto_rvs();
                

                res = numero_rvs-numero;
                primeravez = false;
                vueltas++;
            }else{
                int numero = numerocompleto();
                int numero_rvs = numerocompleto_rvs();

                res = numero_rvs-numero;
                vueltas++;
            }
            if(repdigits()){
                vueltas=8;
                res=6174;
                break;
            }

        }

        if(conskaprekar()){
            res=6174;
            vueltas=0;
            return vueltas;
        }else{
            return vueltas;
        }
    }

    public static boolean conskaprekar(){

        Integer[] numeros_rvs = validante.clone();
        

        String n0 = Arrays.toString(numeros_rvs)
        .replace(",", "")
        .replace("[", "")
        .replace("]", "")
        .replace(" ", "")
        .trim();        
        int ints = Integer.parseInt(n0);
        if(ints==6174){
            return true;
        }else{
            return false;
        }

    }
        

	public static String lecturaLinea() {
		String linea = scanner.nextLine();
		return linea;
	}

    public static Integer[] convIntegers(){


        if(primeravez){
            String str = Ejercicio100.lecturaLinea();


            charord = str.toCharArray();
            Integer[] numeros = new Integer[4];
            int n;
            for(int i = 0; i < charord.length; i++){
                n = charord[i]-'0';
                numeros[i] = n;
            }
            validante = numeros.clone();
            Arrays.sort(numeros);
            input = numeros;
            return numeros;

        }else{
            int resultado = res;
            String str = String.valueOf(resultado);

            charord = str.toCharArray();
            Integer[] numeros = new Integer[4];
            int n;
            for(int i = 0; i < charord.length; i++){
                n = charord[i]-'0';
                numeros[i] = n;
            }
            input = numeros.clone();
            Arrays.sort(numeros);
            return numeros;
        }

    }


    public static int numerocompleto(){
        Integer[] numeros = convIntegers();
        
        String n0 = Arrays.toString(numeros)
        .replace(",", "")
        .replace("[", "")
        .replace("]", "")
        .replace(" ", "")
        .trim();        
        int numero_c = Integer.parseInt(n0);
        return numero_c;
    }

    public static int numerocompleto_rvs(){
        Integer[] numeros_rvs = input.clone();
        Arrays.sort(numeros_rvs, Collections.reverseOrder());
        

        String n0 = Arrays.toString(numeros_rvs)
        .replace(",", "")
        .replace("[", "")
        .replace("]", "")
        .replace(" ", "")
        .trim();        
        int numero_c_rvs = Integer.parseInt(n0);
        return numero_c_rvs;
    }


    public static boolean repdigits(){
        Integer[] numeros = input;

        if(numeros[0] == numeros[1] && numeros[1] == numeros[2] &&numeros[2] == numeros[3]){
            return true;
        }else{
            return false;
        }
    }



}
