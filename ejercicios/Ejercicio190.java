
import java.util.Scanner;

public class Ejercicio190 {
	public static Scanner scanner = new Scanner(System.in);
	public static String CADENA = " ";
	public static String UNO = "1";
	public static void main(String[] var0) {
		try {
			long numerador = 1L;
			long denominador = 0L;

			String[] str = Ejercicio190.lecturaLinea();
			numerador = Long.parseLong(str[0]);
			denominador = Long.parseLong(str[1]);

			while (validarNumeros(numerador, denominador)) {

				long valorFactorial = Long.parseLong(UNO);
				for (long i = denominador + 1; i <= numerador; i++) {
					valorFactorial = valorFactorial * i;
				}

				System.out.println(valorFactorial);

				str = Ejercicio190.lecturaLinea();
				numerador = Long.parseLong(str[0]);
				denominador = Long.parseLong(str[1]);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String[] lecturaLinea() {
		String linea = scanner.nextLine();
		String[] str = linea.split(CADENA);
		return str;
	}

	public static boolean validarNumeros(long numerador, long denominador) {

		if ((numerador > 0 && numerador < 1000000) && (denominador > 0 && denominador < 1000000)
				&& (numerador >= denominador)) {
			return true;
		} else {
			return false;
		}

	}

}
