import java.util.Arrays;
import java.util.Scanner;

/**
 * Constante de Kaprekar
 */
public class Ejercicio100_2 {
    public static Scanner scanner = new Scanner(System.in);
    public static String CERO = "0";
    public static int[] numeros = new int[4];
    public static String entradaTexto;
    public static int res = 0;
    public static boolean primeravez = true;

    public static void main(String[] var0) {
        try{
            int numerovueltas = lecturavueltas();
            int n = 0;
            while(n != numerovueltas){

                lecturanumeros();
                if(verificarlongitud()){
                    break;
                }
                System.out.println(vueltas());
                primeravez = true;
                res = 0;
                n++;
            }
            
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public static int vueltas(){
        int vueltas = 0;

        if(repdigits()){
            vueltas = 8;
            return vueltas;
        }else{
            if(Integer.parseInt(entradaTexto) == 6174){
                return vueltas;
            }else{
                while(res!=6174){
                    if(primeravez){
                        int numero = ordenar();
                        int numeroinv = ordenarinv();
            
                        res = numeroinv-numero;
                        primeravez = false;
                        vueltas++;
                    }else{
                        conversionresultado();
                        int numero = ordenar();
                        int numeroinv = ordenarinv();
                        res = numeroinv-numero;
                        vueltas++;
                    }
                }
        
                return vueltas;
            }
    

        }

    }

    public static int lecturavueltas(){
        int v = 0;
        String linea = scanner.nextLine();
        v = Integer.parseInt(linea);
        return v;
    }

    public static boolean verificarlongitud(){
        if(numeros.length!=4){
            return true;
        }else{
            return false;
        }
    }

    public static void lecturanumeros(){
        String linea = scanner.nextLine();
        //Verificamos que haya introducido formato 4 dígitos XXXX
        if (linea.length() < 4){
            int cerosAnadir = 4 - linea.length();
            for (int i = 0; i < cerosAnadir; i++){
                linea = CERO.concat(linea);
            }
        }

        for(int i = 0; i < linea.length(); i++){
            numeros[i]  = Character.getNumericValue(linea.charAt(i));
        }

        entradaTexto = linea;
    }

    public static void resetArrayNumeros(){
        for(int i = 0; i < numeros.length; i++){
            numeros[i] = 0;
        }
    }

    public static boolean repdigits(){
        if(numeros[0] == numeros[1] &&numeros[1] == numeros[2] &&numeros[2] == numeros[3]){
            return true;
        }else{
            return false;
        }
    }

    public static String conversionresultado(){
        resetArrayNumeros();

        String str = String.valueOf(res);
        char[] charord = str.toCharArray();
        
        int n;
        for(int i = 0; i < charord.length; i++){
            n = charord[i]-'0';
            numeros[i] = n;
        }
        return "";
    }

    public static int ordenar(){
        int ram = 0;
        int comp = 0;

        for(boolean comprobado = false; comprobado!=true;){
            for(int i = 0; i < numeros.length-1;i++){
                if(numeros[i+1]<numeros[i]){
                    ram = numeros[i];
                    numeros[i] = numeros[i+1];  
                    numeros[i+1] = ram;
                }
            }         
            comp++;
            if(numeros.length == comp){
                comprobado = true;
            }
        }


        
        String n0 = Arrays.toString(numeros)
        .replace(",", "")
        .replace("[", "")
        .replace("]", "")
        .replace(" ", "")
        .trim();     
        int numerofinal = Integer.parseInt(n0);
        return numerofinal;
    }


    public static int ordenarinv(){
        int ram = 0;
        int comp = 0;

        for(boolean comprobado = false; comprobado!=true;){
            for(int i = 0; i < numeros.length-1;i++){
                if(numeros[i+1]>numeros[i]){
                    ram = numeros[i];
                    numeros[i] = numeros[i+1];  
                    numeros[i+1] = ram;
                }
            }         
            comp++;
            if(numeros.length == comp){
                comprobado = true;
            }
        }


        

        String n0 = Arrays.toString(numeros)
        .replace(",", "")
        .replace("[", "")
        .replace("]", "")
        .replace(" ", "")
        .trim();     
        int numerofinal = Integer.parseInt(n0);
        return numerofinal;
    }
}
