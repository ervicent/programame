import java.util.*;
public class Ejercicio168 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num;
        int valor = sc.nextInt();
        while (valor != 0) {
            boolean taula[]=new boolean[valor];
            Arrays.fill(taula,false);
            //llegim nums
            for(int i=0;i<valor-1;i++){
                num = sc.nextInt();
                taula[num-1]=true;
            }
            //comprovem quin falla
            int numfalta=0;
            for(int i=0;i<valor && numfalta==0;i++){
                if(!taula[i])
                    numfalta=i+1;
            }
            System.out.println(numfalta);
            valor = sc.nextInt();
        }
    }
}
