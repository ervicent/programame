import java.util.Scanner;

public class Ejercicio474 {
    public static void main(String[] args) {
        int S,A,B,dist1,dist2,distancia;
        Scanner entrada = new Scanner(System.in);
        int casos = entrada.nextInt();
        entrada.nextLine();
        for (int i = 0; i<casos;i++){
            //codi del programa
            S=entrada.nextInt();
            A=entrada.nextInt();
            B=entrada.nextInt();
            dist1=Math.abs(A-S);
            dist2=Math.abs(B-S);
                if((A-S>0 && B-S>0)||(A-S<0 && B-S<0))
                    if (dist1<dist2)
                        distancia=dist2;
                    else
                        distancia=dist1;
                else
                    if (dist1<dist2)
                        distancia=dist1+dist1+dist2;
                    else
                        distancia=dist2+dist2+dist1;

            System.out.println(distancia);
        }
        
    }
}
