import java.util.Scanner;

public class Ejercicio126 {

    public static Scanner scanner = new Scanner(System.in);
    // public static List<Integer> listaPrimo = Ejercicio126.listaPrimos(10000);
    public static String CADENA = " ";
    public static String YES = "YES";
    public static String NO = "NO";

    public static void main(String[] argv) {

        try {
            int primo = 0;
            int numero = 0;

            String[] str = Ejercicio126.lecturaLinea();
            primo = Integer.parseInt(str[0]);
            numero = Integer.parseInt(str[1]);

            while (validarNumeros(numero, primo)) {

                if (Math.abs(numero) >= Math.abs(primo)){
                    System.out.println(YES);
                }
                else {
                    System.out.println(NO);
                }

                str = Ejercicio126.lecturaLinea();
                primo = Integer.parseInt(str[0]);
                numero = Integer.parseInt(str[1]);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String[] lecturaLinea() {
        String linea = scanner.nextLine();
        String[] str = linea.split(CADENA);
        return str;
    }

    public static boolean validarNumeros(int numero, int primo) {
        if (numero < 0 && primo < 0) {
            return false;
        } 
        else if (!esPrimo(primo)) {
            return false;
        } 
        else {
            return true;
        }

    }
        
    public static boolean esPrimo(int numero) {
        if (numero == 1) {
            return true;
        } 
        else {
            int maximo = numero / 2;
            for (int i = 2; i <= maximo; i++) {
                if (numero % i == 0) {
                    return false;
                }
            }
            return true;
        }
    }

    // public static ArrayList<Integer> divisoresNumero(int numero){
    //     ArrayList <Integer> arrayDivisores = new ArrayList<Integer>();
    //     int maximo = numero / 2;
    //     for (int i = 2; i <= maximo; i++) {
    //         if (numero % i == 0) {
    //             arrayDivisores.add(i);
    //         }
    //     }

    //     return arrayDivisores;
    // }

    // public static List<Integer> listaPrimos(int numero){
    //     int maximoPrimo = numero;
    //     ArrayList<Integer> array = new ArrayList<Integer>();
    //     array.add(1);
    //     for (int i = 2; i < maximoPrimo; i++) {
    //         if (esPrimo(i)){
    //             array.add(i);                
    //         }
    //     }
    //     for (Integer i : array) {
    //         System.out.println(i);
    //     }
    //     return array;
    // }
}
