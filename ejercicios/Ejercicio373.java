import java.util.Scanner;

public class Ejercicio373 {
    public static Scanner scanner = new Scanner(System.in);
    public static void main(String[] var0){
        try{
            int casos = casosprueba();
            while(casos!=0){
                int cubos = contarcubos();
                if(cubos == -505){
                    casos = 0;
                    break;
                }else{
                    System.out.println(cubos);
                }
                casos--;
            }
        }catch(Exception e){
            System.out.println(e.getCause());
        }
    }
    public static int contarcubos(){
        int cubodim = scanner.nextInt();
        cubodim = cubodim*cubodim*cubodim;
        if(cubodim>2&&cubodim<1000000){
            return cubodim;
        }else{
            return -505;
        }
    }

    public static int casosprueba(){
        int casos = scanner.nextInt();
        return casos;
    }
}
