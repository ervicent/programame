import java.util.Scanner;

public class Ejercicio428 {

    public static void main(String[] args) {
        int num, temp, modul, conta;
        Scanner entrada = new Scanner(System.in);
        int casos = entrada.nextInt();
        entrada.nextLine();
        for (int i = 0; i < casos; i++) {
            //codi del programa
            conta = 0;
            num = entrada.nextInt();
            do {
                temp = num / 5;
                modul = num % 5;
                num = temp;
                if (modul == 4) {
                    conta++;
                }
            } while (num > 5);
            if(num==4)
                conta++;
            if (conta>1)
                System.out.println("SI");
            else
                System.out.println("NO");
        }
    }
}
