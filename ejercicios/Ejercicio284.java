import java.util.Arrays;
import java.util.Scanner;

public class Ejercicio284 {
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] var0) {
        try{
            int casos = casosprueba();
            while(casos!=0){
                String deuda = deuda();
                if(deuda == "null"){
                    break;
                }else{
                    System.out.println(deuda);
                    casos--;    
                }
            }
        }catch(Exception e){
            System.out.println(e.getCause());
        }
    }

    public static int casosprueba(){
        int casos = scanner.nextInt();
        return casos;
    }

    public static int[] entrada(){
        int[] entrada = new int[2];
        entrada[0] = scanner.nextInt();
        entrada[1] = scanner.nextInt();
        return entrada;
    }

    public static String deuda(){
        int[] entrada = entrada();
        int[] monedas = new int[8];
        int[] monedas_posibles = {200,100,50,20,10,5,2,1};
        if(entrada[0]>500000||entrada[1]>500000||entrada[0]<0||entrada[1]<0){
            return "null";
        }else{
            int deuda = entrada[1]-entrada[0];
            if(deuda<0){
                int faltante = deuda*-1;
                String deud = "DEBE "+faltante;
                return deud;
            }else{
                for(int i = 0; i<monedas.length; i++){
                    if(deuda-monedas_posibles[i]>-1){
                            monedas[i]++;
                            deuda = deuda-monedas_posibles[i];
                            while(deuda-monedas_posibles[i]>-1){
                                monedas[i]++;
                                deuda = deuda-monedas_posibles[i];
                            }    
                            
                    }          
            }
    
            String monedatx = Arrays.toString(monedas)
            .replace(",",  "")
            .replace("[", "")
            .replace("]", "")
            .replace(" ", " ")
            .trim();     
            
            return monedatx;
    
        }
        }




    }
}
