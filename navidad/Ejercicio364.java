package navidad;

import java.util.Arrays;
import java.util.Scanner;

public class Ejercicio364 {
    public static Scanner scanner = new Scanner(System.in);
    public static void main(String[] var0){
        try{
            String encripto = "";
            while(!encripto.equals("FIN")){
                encripto = encriptado();
                if(encripto.equals("FIN")){
                    break;
                }else{
                    System.out.println(encripto);
                }
            }
        }catch(Exception e){
            System.out.println(e.getCause());
        }
    }

    

    public static String escritura(){
        String texto = scanner.nextLine();
        return texto;
    }
    public static String encriptado(){
        String texto = escritura();
        if(texto.equals("FIN") || texto.length()>100 ){
            texto = "FIN";
            return texto;
        }else{
            
            String[] textoencriptado = new String[texto.length()];
            char ori;
            for(int i=0;i<texto.length(); i++){
    
                ori = texto.charAt(i);
                if(ori==32){
                    textoencriptado[i]=String.valueOf((char)ori);
                }
                else if(ori>64&&ori<91){
                    int orig = ori+1;
                    if(ori==90){
                        char A ='A';
                        textoencriptado[i]=String.valueOf((char)A);
                    }else{
                        textoencriptado[i]=String.valueOf((char)orig);
                    }
                }else{
                    texto = "FIN";
                    return texto;        
                }

            }
            String encriptado = Arrays.toString(textoencriptado)
            .replace(", ",  "")
            .replace("[", "")
            .replace("]", "")
            .replace(" ", " ")
            .trim();   
            return encriptado;
        }

    }
}
