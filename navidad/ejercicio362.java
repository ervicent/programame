package navidad;
import java.util.Scanner;

public class ejercicio362 {
    public static Scanner scanner = new Scanner(System.in);
    public static void main(String[] var0){
        try{
            int vueltas = scanner.nextInt();
            for(int i = 0; i < vueltas; i++){
                byte[] fecha = new byte[2];
                fecha[0] = scanner.nextByte();
                fecha[1] = scanner.nextByte();
                String text = fecha[0] == 25 && fecha[1] == 12 ? "SI" : "NO";
                System.out.println(text);
            }
        }catch(Exception e){
            System.out.println(e.getCause());
        }
    }
}
